# Changelog

## [v1.3.0]

- Updated maven-parent-1.2.0
- Updated to dataminer-1.9.1


## [v1.2.2] - 2020-11-11

- Config path in addAlgorithm


